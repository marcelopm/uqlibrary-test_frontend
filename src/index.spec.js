import 'babel-polyfill'

import React from 'react';
import ReactDOM from 'react-dom';
import Root from './containers/Root'

/**
 * App rendering test - check if the app is rendering correctly
 */
it('renders without crashing', () => {
  const div = document.createElement('div');
  ReactDOM.render(<Root />, div);
});