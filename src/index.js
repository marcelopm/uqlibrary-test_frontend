import 'babel-polyfill'

import React from 'react'
import { render } from 'react-dom'
import Root from './containers/Root'

/**
 * Create the app root and define to which HTML tag
 * in public/index.html should be attached to
 */
render(
    <Root />,
    document.getElementById('root')
)