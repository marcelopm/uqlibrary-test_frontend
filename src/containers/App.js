import React, { Component, PropTypes } from 'react'
import { connect } from 'react-redux'
import { Table, TableBody, TableHeader, TableHeaderColumn, TableRow, TableRowColumn } from 'material-ui/Table';
import Paper from 'material-ui/Paper';
import RaisedButton from 'material-ui/RaisedButton';
import LinearProgress from 'material-ui/LinearProgress';
// required by mui
import MuiThemeProvider from 'material-ui/styles/MuiThemeProvider';
import injectTapEventPlugin from 'react-tap-event-plugin';
injectTapEventPlugin();

// actions 
import * as dsActs from '../redux/actions/datastore'
import * as locationActs from '../redux/actions/location'

// components
import SearchableTable from '../components/SearchableTable'
import Dialog from '../components/Dialog'
import LibraryTable from '../components/LibraryTable'

/**
 * App container is used combine all the components together
 */
class App extends Component {
    /**
     * Once mounted, dispatch a datastore request action
     */
    componentDidMount() {
        this.dispatch(dsActs.request())
    }

    /**
     * Event handlers
     */
    onRefresh(e) {
        this.dispatch(dsActs.request())
    }

    onLocationOpen(location, displaying) {
        // in case the location object is valid
        if (location && location.lid && !displaying) {
            // dispatch a location select action
            this.dispatch(locationActs.select(location))
        }
    }

    /**
     * Hooked to onOpen location dialog, so it gets fired once the Dialog component mounts
     */
    onLocationSelected(e) {
        // make sure it gets dispached after a possible touch delay on mobile
        setTimeout(() => this.dispatch(locationActs.displaying()), 400)
    }

    onLocationClose(e) {
        e.preventDefault()
        this.dispatch(locationActs.clear())
    }    

    /**
     *  Dispatch helper method
     */
    dispatch() {
        this.props.dispatch(...arguments)
    }

    /**
     * Return table's header containing app's title and a refresh data fetch button
     * in case fetching props is not true
     */
    renderHeader() {
        const { ds } = this.props

        return (
            <TableHeader adjustForCheckbox={false} displaySelectAll={false}>
                <TableRow>
                    <TableHeaderColumn style={{ height: 100 }}>
                        <h1>UQ Library - Locations</h1>
                    </TableHeaderColumn>
                    <TableHeaderColumn className="button-holder">
                        {!ds.fetching &&
                            <RaisedButton label="Refresh" primary={true} onClick={(e) => this.onRefresh(e)} />
                        }
                    </TableHeaderColumn>
                </TableRow>
            </TableHeader>
        )
    }

    /**
     * Return table's body with conditional rendering on:
     * - a progress indicator in case fetching prop true
     * - a retry message in case of empty data or error
     * - a SearchableTable component with the fetched data in case none the above is true
     * - a dialog containing a LibraryTable component in case location prop has an lid attribute 
     */
    renderBody() {
        const { ds, location } = this.props

        return (
            <TableBody displayRowCheckbox={false} deselectOnClickaway={false}>
                <TableRow>
                    <TableRowColumn>
                        {ds.fetching &&
                            <LinearProgress mode="indeterminate" />
                        }
                        {!ds.fetching &&
                            (!ds.data.length ||ds.error ? (
                                "No locations?! please try again"
                            ) : (
                                    <div style={{ opacity: ds.fetching ? 0.5 : 1 }}>
                                        <SearchableTable data={ds.data} onRowSelection={(item) => this.onLocationOpen(item, location.displaying)} />
                                        {location.current.lid &&
                                            <Dialog
                                                onOpen={(e) => this.onLocationSelected(e)}
                                                onClose={(e) => this.onLocationClose(e)}
                                                title={location.current.name}>
                                                <LibraryTable data={location.current} />
                                            </Dialog>
                                        }
                                    </div>
                                )
                            )}
                    </TableRowColumn>
                </TableRow>
            </TableBody>
        )
    }

    /**
     * Render the app using all other components
     */
    render() {
        return (
            <MuiThemeProvider>
                <Paper zDepth={2} style={{ 'maxWidth': '600px', margin: '0 auto' }}>
                    <Table selectable={false}>
                        {this.renderHeader()}
                        {this.renderBody()}
                    </Table>
                </Paper>
            </MuiThemeProvider>
        )
    }
}

/**
 * Definition of the app state attributes
 */
App.propTypes = {
    ds: PropTypes.object.isRequired,
    location: PropTypes.object.isRequired,
    dispatch: PropTypes.func.isRequired,
}

/**
 * Redux state's attributes to component's props mapping method
 * 
 * This used to bind app state updates with the component props,
 * So it will know when to re-render the UI 
 */
function mapStateToProps(state) {
    const { fetching, data, error} = state.datastore
    const { current, displaying } = state.location

    return {
        ds: {
            fetching, data, error
        },
        location: {
            current, displaying
        }
    }
}

// connect the mapping definition above to the app component
export default connect(mapStateToProps)(App)