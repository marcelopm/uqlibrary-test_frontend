import React, { Component } from 'react'
import { Provider } from 'react-redux'
import configureStore from '../redux/store'
import App from './App'

// get our app store
const store = configureStore()

/**
 * and set the app wrapped to a provider with the store
 * so that all the necessary app state to component props
 * binding will work properly and seamless
 */
export default class Root extends Component {
    render() {
        return (
            <Provider store={store}>
                <App />
            </Provider>
        )
    }
}