import * as actions from './location'
import * as types from '../constants/actions'

/**
 * The location actions tests
 */
describe('location actions', () => {
    it('select() should create ' + types.LOCATION_SELECT + ' action', () => {
        // location mock
        const location = {
            lid: 1234,
            name: 'test'
        }

        expect(actions.select(location)).toEqual({
            type: types.LOCATION_SELECT,
            payload: location
        })
    })

    it('clear() should create ' + types.LOCATION_SELECTED + ' action', () => {
        expect(actions.displaying()).toEqual({
            type: types.LOCATION_SELECTED,
        })
    })

    it('clear() should create ' + types.LOCATION_CLEAR + ' action', () => {
        expect(actions.clear('active')).toEqual({
            type: types.LOCATION_CLEAR,
        })
    })
})