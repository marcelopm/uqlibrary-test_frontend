import * as types from '../constants/actions'
import { API_URL, API_ENDPOINT_LIBRARY_HOURS } from '../constants/config'
import fetch from 'isomorphic-fetch';

// keeping it as an object so it can be used reused for testing too
export const formated = {
    // returns a formated request action
    request: () => {
        return {
            type: types.FETCH_PENDING,
        }
    },
    //  returns a formated response action
    response: (response) => {
        return {
            type: types.FETCH_FULFILLED,
            payload: response
        }
    },
    //  returns a formated error action
    error: (error) => {
        return {
            type: types.FETCH_REJECTED,
            payload: error
        }
    }
}

/**
 * The datastore request method responsible for fetching data from the API
 */
export const request = (endpoint = API_ENDPOINT_LIBRARY_HOURS, url = API_URL) => {
    return (dispatch, getState) => {
        // let the reducer know a fetch is going on 
        dispatch(formated.request())

        // fetch the data and dispatch response or error
        return fetch(url + endpoint)
            .then(response => response.json())
            .then(json => dispatch(response(json.locations)))
            .catch(response => dispatch(error(response)))
    }
}

// simply expose the formated actions defined above
export const response = response => formated.response(response)
export const error = error => formated.error(error)