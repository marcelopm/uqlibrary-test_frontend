import * as types from '../constants/actions'

export const select = (location) => ({
    type: types.LOCATION_SELECT,
    payload: location
})

export const displaying = () => ({
    type: types.LOCATION_SELECTED,
})

export const clear = () => ({
    type: types.LOCATION_CLEAR
})