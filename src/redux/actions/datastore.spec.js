import configureMockStore from 'redux-mock-store'
import nock from 'nock'
import thunk from 'redux-thunk'
import * as actions from './datastore'
import * as types from '../constants/actions'
import expect from 'expect' // using expect from redux doc page instead of the one from jest's one
import { API_URL, API_ENDPOINT_LIBRARY_HOURS } from '../constants/config'

// set up a mock store for the request action
const middlewares = [thunk]
const mockStore = configureMockStore(middlewares)

/**
 * Datastore actions tests
 */
describe('datastore actions', () => {
    afterEach(() => {
        nock.cleanAll()
    })

    it('request() should create ' + types.FETCH_PENDING + ', ' + types.FETCH_FULFILLED + ' actions', () => {
        const response = { locations: [{}] }

        // mock a response using the nock library
        nock(API_URL)
            .get(API_ENDPOINT_LIBRARY_HOURS)
            .reply(200, response)

        // create a mock store
        const store = mockStore(response)

        // and dispatch a request action
        return store.dispatch(actions.request())
            .then(() => {
                // then we expect to see request and response formated actions
                expect(store.getActions()).toEqual([
                    actions.formated.request(),
                    actions.formated.response(response.locations),
                ])
            })

    })

    it('response() should create ' + types.FETCH_FULFILLED + ' action', () => {
        const input = {
            locations: []
        }

        expect(actions.response(input)).toEqual(actions.formated.response(input))
    })

    it('error() should create ' + types.FETCH_REJECTED + ' action', () => {
        const input = new Error('test')

        expect(actions.error(input)).toEqual(actions.formated.error(input))
    })
})