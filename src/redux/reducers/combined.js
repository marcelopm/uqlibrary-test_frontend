import { combineReducers } from 'redux'

import datastore from './datastore'
import location from './location'

// root reducer is a combination of all other reducers
const rootReducer = combineReducers({
    datastore,
    location
})

export default rootReducer