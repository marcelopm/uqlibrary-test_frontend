import * as types from '../constants/actions'

/**
 * location states, used in here and for tests too
 */
export const states = {
    initialState: {
        current: {},
        displaying: false,
    },
    [types.LOCATION_SELECT]: (state, action) => {
        return {
            ...state,
            current: action.payload,
        }
    },
    [types.LOCATION_SELECTED]: (state, action) => {
        return {
            ...state,
            displaying: true,
        }
    },
    [types.LOCATION_CLEAR]: (state, action) => {
        return {
            ...state,
            current: states.initialState.current,
            displaying: false
        }
    },
}

/**
 * Our reducer is just a simple method that wraps a switch case to
 * define with state should be returned next using the above methods
 */
export const reducer = (state = states.initialState, action) => {
    switch (action.type) {
        case types.LOCATION_SELECT:
            return states[types.LOCATION_SELECT](state, action)
        case types.LOCATION_SELECTED:
            return states[types.LOCATION_SELECTED](state, action)
        case types.LOCATION_CLEAR:
            return states[types.LOCATION_CLEAR](state, action)
        default:
            return state
    }
}

export default reducer