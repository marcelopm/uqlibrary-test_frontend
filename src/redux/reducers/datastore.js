import * as types from '../constants/actions'

/**
 * datastore states, used in here and for tests too
 */
export const states = {
    initialState: {
        fetching: false,
        error: false,
        data: []
    },
    [types.FETCH_PENDING]: (state) => {
        return {
            ...state,
            fetching: true,
        }
    },
    [types.FETCH_FULFILLED]: (state, action) => {
        return {
            ...state,
            fetching: false,
            error: false,
            data: action.payload
        }
    },
    [types.FETCH_REJECTED]: (state, action) => {
        return {
            ...state,
            fetching: false,
            error: action.payload
        }
    },
}

/**
 * Our reducer is just a simple method that wraps a switch case to
 * define with state should be returned next using the above methods
 */
export const reducer = (state = states.initialState, action) => {
    switch (action.type) {
        case types.FETCH_PENDING:
            return states[types.FETCH_PENDING](state)
        case types.FETCH_FULFILLED: 
            return states[types.FETCH_FULFILLED](state, action)
        case types.FETCH_REJECTED:
            return states[types.FETCH_REJECTED](state, action)
        default:
            return state
    }
}

export default reducer