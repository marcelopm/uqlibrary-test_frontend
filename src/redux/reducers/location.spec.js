import * as types from '../constants/actions'
import * as actions from '../actions/location'
import { states, reducer } from './location'
import expect from 'expect'

/**
 * location actions reducer tests
 * 
 * Variable naming:
 * - prevState: current state or initial state
 * - nextState: the after state, once is processed by a given action reducer method
 */
describe('location reducer', () => {
    it('should return the initial state', () => {
        const prevState = states.initialState;
        const action = { type: null };
        const nextState = states.initialState

        expect(reducer(prevState, action)).toEqual(nextState)
    })

    it('should handle ' + types.LOCATION_SELECT, () => {
        // location mock
        const location = { lid: 1234, name: 'test' }
        const prevState = { ...states.initialState };
        const action = actions.select(location)
        const nextState = states[types.LOCATION_SELECT](prevState, action)

        expect(reducer(prevState, action)).toEqual(nextState)
    })

    it('should handle ' + types.LOCATION_SELECTED, () => {
        // location mock
        const location = { lid: 1234, name: 'test' }
        const prevState = { ...states.initialState, current: location };
        const action = actions.displaying()
        const nextState = states[types.LOCATION_SELECTED](prevState, action)

        expect(reducer(prevState, action)).toEqual(nextState)
    })

    it('should handle ' + types.LOCATION_CLEAR, () => {
        // location mock
        const location = { lid: 1234, name: 'test' }
        const prevState = { ...states.initialState, current: location, displaying: true }
        const action = actions.clear()
        const nextState = states[types.LOCATION_CLEAR](prevState, action);

        expect(reducer(prevState, action)).toEqual(nextState)
    })
})