import * as types from '../constants/actions'
import * as actions from '../actions/datastore'
import { states, reducer } from './datastore'
import expect from 'expect'

/**
 * datastore actions reducer tests
 * 
 * Variable naming:
 * - prevState: current state or initial state
 * - nextState: the after state, once is processed by a given action reducer method
 */
describe('datastore reducer', () => {
    it('should return the initial state', () => {
        const prevState = states.initialState;
        const action = { type: null };
        const nextState = states.initialState

        expect(reducer(prevState, action)).toEqual(nextState)
    })

    it('should handle ' + types.FETCH_PENDING, () => {
        const prevState = { ...states.initialState, fetching: true };
        const action = actions.formated.request()
        const nextState = states[types.FETCH_PENDING](prevState, action);

        expect(reducer(prevState, action)).toEqual(nextState)
    })

    it('should handle ' + types.FETCH_FULFILLED, () => {
        // locations mock
        const data = [{ lid: 123, name: test }];
        const prevState = { ...states.initialState, fetching: true };
        const action = actions.formated.response(data)
        const nextState = states[types.FETCH_FULFILLED](prevState, action);

        expect(reducer(prevState, action)).toEqual(nextState)
    })

    it('should handle ' + types.FETCH_REJECTED, () => {
        const prevState = { ...states.initialState };
        const action = actions.formated.error(true)
        const nextState = states[types.FETCH_REJECTED](prevState, action);

        expect(reducer(prevState, action)).toEqual(nextState)
    })
})