import {getHost} from '../../utils/url'

export const API_URL = getHost()
export const API_ENDPOINT_LIBRARY_HOURS = '/api/v2/library_hours/week'
export const APP_LOG_FILE = '/tmp/app.log'