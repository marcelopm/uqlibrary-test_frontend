import { createStore, applyMiddleware } from 'redux'
import thunk from 'redux-thunk'
import logger from 'redux-logger'
import combined from './reducers/combined'

/**
 * Method tha returns our app store containing its reducers
 * and necessary middlewares
 * 
 * Its initial state is defined within reducers
 */
export default function configureStore(preloadedState) {
    return createStore(
        combined,
        preloadedState,
        applyMiddleware(
            thunk,
            // uncomment the bellow for app action and states debugging
            // logger()
        )
    )
}