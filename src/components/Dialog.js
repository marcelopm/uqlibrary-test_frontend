import React from 'react';
import MUIDialog from 'material-ui/Dialog';
import FlatButton from 'material-ui/FlatButton';

const styles = {
    dialog: {
        width: '90%',
        maxWidth: 'none',
    },
};

/**
 * A simple dialog component that wraps its children and provide a hook to its onClose event 
 */
export default class Dialog extends React.Component {
    /**
     * Once mounted, if defined, call the hooked onOpen method
     */
    componentDidMount() {
        if (this.props.onOpen) {
            this.props.onOpen()
        }
    }

    handleClose = (e) => {
        // proxy the props onClose methods with the event object
        this.props.onClose(e);
    };

    render() {
        // close buttom
        const actions = [
            <FlatButton
                label="Close"
                primary={true}
                onTouchTap={this.handleClose}
                />
        ];

        return (
            <MUIDialog
                className="dialog"
                actions={actions}
                modal={true}
                open={true}
                onRequestClose={this.handleClose}
                autoScrollBodyContent={true}
                contentStyle={styles.dialog}
                title={this.props.title}
                >
                {this.props.children}
            </MUIDialog>
        );
    }
}