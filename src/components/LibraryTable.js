import React from 'react';
import moment from 'moment';
import _ from 'lodash';
import { Table, TableBody, TableHeader, TableHeaderColumn, TableRow, TableRowColumn } from 'material-ui/Table';

/**
 * Library info table. Component used to render the information for a given library object
 * 
 * Layout base on: https://web.library.uq.edu.au/locations-hours/opening-hours
 */
export default class LibraryTable extends React.Component {
    /**
     * Return table headers containing the days on the week based on a given week object
     */
    renderHeaders(week) {
        return (
            <TableHeader adjustForCheckbox={false} displaySelectAll={false}>
                <TableRow>
                    <TableHeaderColumn />
                    {Object.entries(week).map((day, index) => {
                        return (
                            <TableHeaderColumn key={index}>
                                {moment(day[1]['date']).format('MMM DD')}<br />
                                {moment(day[1]['date']).format('ddd')}
                            </TableHeaderColumn>
                        )
                    })}
                </TableRow>
            </TableHeader>
        )
    }

    /**
     * Return table rows containing the library's departments and its week hours
     */
    renderDepartments(library, week) {
        return library.departments.map((department, index) =>
            <TableRow key={index}>
                <TableRowColumn>{department.name}</TableRowColumn>
                {department.weeks.map((week, index) => 
                    Object.entries(week).map((day, index) =>
                        <TableRowColumn key={index}>
                            <div className="s-show">
                                {moment(day[1]['date']).format('dddd')}
                            </div>
                            <div>
                                {day[1].rendered}
                            </div>
                        </TableRowColumn>
                    )
                )}
            </TableRow>
        )
    }

    /**
     * Render the table combining the table headers and rows
     */
    render() {
        let library = this.props.data;
        let week = _.get(library, 'weeks[0]', {});

        return (
            <Table selectable={false} className="library-table">
                {this.renderHeaders(week)}
                <TableBody displayRowCheckbox={false} deselectOnClickaway={false}>
                    {this.renderDepartments(library, week)}
                </TableBody>
            </Table>
        )
    }
}