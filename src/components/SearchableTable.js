import React, { Component } from 'react'
import { Table, TableBody, TableHeader, TableHeaderColumn, TableRow, TableRowColumn } from 'material-ui/Table';
import TextField from 'material-ui/TextField';

/**
 * Searchable Data Table Component inspired on mui-data-table:
 * 
 * https://github.com/andela-cdaniel/mui-data-table/blob/master/src/lib/mui-data-table.js
 * 
 * It was not possible to use mui-data-table due to its lack of hooks for onRowSelection
 * 
 *  (onRowSelection is described at: http://www.material-ui.com/v0.15.1/#/components/table)
 */
export default class SearchableTable extends Component {
    constructor(props) {
        super(props)

        // the table single state property is its dataset
        this.state = {
            data: props.data,
        };

        if (props.onRowSelection) {
            /**
             * Wrap the method provided through props, so the selected data object can be accessed
             * instead of the table row's index
             * 
             * This is necessary since MaterialUI only provides the index of the selected row
             * @see http://www.material-ui.com/v0.15.1/#/components/table
             */
            this.onRowSelection = (index) => props.onRowSelection.call(this, this.state.data[index])
        }
    }

    /**
     * Empty func, used in case none is provided within props
     */
    onRowSelection = () => null

    /**
     * Filter an array of objects for a given word and keys that should be searched
     * 
     * Based on searchData:
     *  https://github.com/andela-cdaniel/mui-data-table/blob/master/src/lib/mui-data-table.js#LC230
     * 
     * return array
     */
    onSearch(e) {
        // get the searchable keys from the first dataset in case it's not provided
        const searchableKeys = this.props.searchableKeys || Object.keys(this.props.data && this.props.data[0] || {}).join('|');
        const query = e.target.value;
        const data = this.props.data;

        // filter the data object according to the give search term
        let result = this.filter(searchableKeys, query, data);

        // make sure to update our state data, so everything necessary gets refreshed
        this.setState({
            data: result
        });
    }

    /**
     * Filter an array of objects for a given word and keys that should be searched
     * 
     * Copied & altered from https://github.com/andela-cdaniel/mui-data-table/blob/master/src/utils/search.js
     * 
     * return array
     */
    filter = (searchableKeys, query, data = []) => {
        // no query
        if (query.length < 1) return data;

        const result = [];
        const regex = new RegExp(query, 'i');
        const keys = searchableKeys.split('|');

        // go through every records within the given data
        data.forEach((item) => {
            // and through every record's attributes
            for (let i = 0; i < keys.length; i += 1) {
                // and try to match the query term with its value
                if (String(item[keys[i]]).match(regex)) {
                    // in case it matches, push it to the result array 
                    result.push(item);
                    break;
                }
            }

        });

        // return the filtered data
        return result;
    }

    /**
     * Return the filter widget in case there are data to be filter
     */
    renderFilter() {
        const style = { border: 'none' }
        if (!!this.props.data && !!this.props.data.length)
            return <TableHeader adjustForCheckbox={false} displaySelectAll={false} style={style}>
                <TableRow style={style}>
                    <TableHeaderColumn>
                        <TextField
                            hintText="search. pipes supported i.e this|that"
                            onChange={(e) => this.onSearch(e)}
                            fullWidth={false} />
                    </TableHeaderColumn>
                </TableRow>
            </TableHeader>
    }

    /**
     * Return the table row based on the given array of data
     * 
     * TODO:
     *  - make number of columns dynamic:
     *  - include hidden columns option
     */
    renderRows(data = []) {
        return (data).map((item, index) =>
            <TableRow key={index}>
                <TableRowColumn>{item.name}</TableRowColumn>
            </TableRow>
        );
    }

    /**
     * Render the table component with its parts
     */
    render() {
        return (
            <Table selectable={true} onRowSelection={(index) => this.onRowSelection(index)} className="searchable-table">
                {this.renderFilter()}
                <TableBody displayRowCheckbox={false} deselectOnClickaway={true}>
                    {this.renderRows(this.state.data)}
                </TableBody>
            </Table>
        )
    }
}