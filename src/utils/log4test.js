import toSource from 'tosource-polyfill'
import { APP_LOG_FILE } from '../redux/constants/config'

/**
 * Utility methods
 */

/**
 * Log method useful for debugging jest test since
 * console.[log,warn,error] is not available
 * 
 * It tries to transform its arguments into human readable
 * information using toSource mozilla's method 
 */
export const log = (...args) => {
    if (args.length) {
        // log file can is defined by config
        require('fs').appendFileSync(APP_LOG_FILE, toSource(args) + "\n", { 'encoding': 'utf8' });
    }
}

export default log