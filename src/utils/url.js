/**
 * Returns the server hostname based on env vars or through OS library
 * 
 * defaults to 'localhost'
 */
export const getHostname = () => {
    let hostname = process.env.HOST || process.env.HOSTNAME || 'localhost'

    try {
        if (process.env.NODE_ENV === 'production' && hostname === 'localhost' ) {
            const os = require('os');
            hostname = os.hostname();
        }
    } catch (e) {
    }

    return hostname;
}


/**
 * Returns the server port
 * 
 * Defaults to 3000 for non prod envs
 */
export const getPort = () => {
    let port = process.env.port || process.env.PORT

    if (port === 80) {
        return ''
    } else if (process.env.NODE_ENV !== 'production') {
        return 3000
    }

    return port
}

/**
 * Returns the server public url
 */
export const getHost = () => {
    let host = `//${getHostname()}`
    let port = getPort()

    if (port) {
        host = `${host}:${port}`
    }

    if (process.env.NODE_ENV === 'test') {
        // fecth doesn't seem to like non absolute paths when tested by jest
        host = `http:${host}`
    }

    return host
}