# UQ Library Frontend test - JS App

This project was created as part of the requirement from the [UQ Library frontend test](https://github.com/uqlibrary/work-test#frontend-test-javascript-app)

The project is a simple SPA app, manly built with [ReactJs](https://facebook.github.io/react/), [Redux] and a few other libraries.

Once up and running, the app fetches information from an [API](https://github.com/uqlibrary/work-test/blob/master/api/library_hours.md) about [UQ libraries locations hours](https://web.library.uq.edu.au/locations-hours/opening-hours) and display them on a searchable list. Once an item of the list is selected, a table containing information about the opening hours for that location is displayed. The popup then can be closed, so other items can be selected.


Demo: [https://uqlibrary-test-frontend.herokuapp.com](https://uqlibrary-test-frontend.herokuapp.com/)


![](app.gif)

# Quick Start

## Get code

Checkout master:

```bash
git clone https://bitbucket.org/marcelopm/uqlibrary-test_frontend {dir}
```

## Running it
The project requires [npm], so make sure the latest version is installed before anything.

Once that's done, the dependencies can be installed and the app started by running the following command:

```bash
$ cd {dir}
$ npm install
$ npm start
```

That should open a tab on your browser with the app's main page. Otherwise, it can be accessed manually by opening [http://localhost:3000]

# Table of Contents

* [Tech](#tech)
* [Installation](#installation)
* [Configuration](#configuration)
* [Folder Structure](#folder-structure)
* [Available Scripts](#available-scripts)
    * [npm start](#npm-start)
    * [npm test](#npm-test)
    * [npm run build](#npm-run-build)
    * [npm run eject](#npm-run-eject)
* [Notes](#notes)
* [License](#license)

# Tech

The project was bootstrapped with [Create React App]. Below you will find some information on how to perform common tasks. You can find further information on it in [here](https://github.com/facebookincubator/create-react-app/blob/master/packages/react-scripts/template/README.md).

It also depends on:

- [redux]
- [redux-thunk]
- [lodash]
- [material-ui]
- [moment]

And also for testing and debugging on:

- [expect]
- [nock]
- [redux-logger]
- [redux-mock-store]
- few others

# Installation

Checkout master:

```bash
$ git clone https://bitbucket.org/marcelopm/uqlibrary-test_frontend {dir}
$ cd {dir}
```

The project requires [npm], so make sure the latest version is installed before anything. Once that's done, the dependencies can be installed

```bash
$ npm install
```
# Folder structure

Once everything is installed, the project folder should look like this:

```
{dir}/
  README.md
  node_modules/
  package.json
  public/
    index.html
    favicon.ico
  src/
    index.js
    index.spec.js
    components/
    containers/
    redux/ <--- redux related files
      actions/...
      reducers/...
      constants/...
      store.js
    utils.js  
```

# Configurations

There shouldn't be need for changing any configuration. Once installed properly, everything should be configured out of the box. In case any tweaking is needed, please refer to config.js file in:

```
{dir}/
  ...
  redux/
    constants/
      config.js
```


Note: the API_URL key value points to localhost because it uses the [Create React App proxy](https://github.com/facebookincubator/create-react-app/blob/master/packages/react-scripts/template/README.md#proxying-api-requests-in-development) functionality to access third party APIs. The proxy is configured on package.json file, on the root of the app's dir:


```json
"proxy": "https://app.library.uq.edu.au"
``` 

## Available Scripts

In the project directory, you can run:


## npm start

```bash
$ npm start
```

Runs the app in the development mode.

Open [http://localhost:3000] to view it in the browser.

The page will reload if you make edits.

You will also see any lint errors in the console.

## npm test

```bash
$ npm test
```

Launches the test runner in the interactive watch mode.

See the section about [running tests](https://github.com/facebookincubator/create-react-app/blob/master/packages/react-scripts/template/README.md#running-tests) for more information.

## npm run build

```bash
$ npm run build
```

Builds the app for production to the `build` folder.

It correctly bundles React in production mode and optimizes the build for the best performance.

The build is minified and the filenames include the hashes.

Your app is ready to be deployed!

## npm run eject

```bash
$ npm run eject
```

**Note: this is a one-way operation. Once you `eject`, you can't go back!**

If you aren't satisfied with the build tool and configuration choices, you can `eject` at any time. This command will remove the single build dependency from your project.

Instead, it will copy all the configuration files and the transitive dependencies (Webpack, Babel, ESLint, etc) right into your project so you have full control over them. All of the commands except `eject` will still work, but they will point to the copied scripts so you can tweak them. At this point you're on your own.

See section about [`ejecting`](https://github.com/facebookincubator/create-react-app/blob/master/packages/react-scripts/template/README.md#npm-run-eject) for more information.

# Notes

Given the nature of the project, for the sake of the simplicity on making the app responsive and tweaking a few elements of the UI, in-line styling was used in public/index.html

Ideally, a better practices should be adopted, following react apps styling best practices, including PostCSS, cssnext, CSS Modules and others.

[//]: # (These are reference links used in the body of this note and get stripped out when the markdown processor does its job. There is no need to format nicely because it shouldn't be seen. Thanks SO - http://stackoverflow.com/questions/4823468/store-comments-in-markdown-syntax)

# License

[![Creative Commons Licence](https://i.creativecommons.org/l/by-nc-sa/4.0/88x31.png)](http://creativecommons.org/licenses/by-nc-sa/4.0/)  
UQ Library Backend Test - JS App service by [Marcelo Moises](https://bitbucket.org/marcelopm/uqlibrary-test_frontend/) is licensed under a [Creative Commons Attribution-NonCommercial-ShareAlike 4.0 International License](http://creativecommons.org/licenses/by-nc-sa/4.0/).  
Based on a work at [https://bitbucket.org/marcelopm/uqlibrary-test_frontend/](https://bitbucket.org/marcelopm/uqlibrary-test_frontend/).

[Create React App]: <https://github.com/facebookincubator/create-react-app>
[redux]: <http://redux.js.org>
[redux-thunk]: <https://github.com/gaearon/redux-thunk>
[lodash]: <https://github.com/lodash/lodash>
[material-ui]: <https://github.com/callemall/material-ui>
[moment]: <https://github.com/moment/moment>
[expect]: <https://github.com/mjackson/expect>
[nock]: <https://github.com/node-nock/nock>
[redux-logger]: <https://github.com/evgenyrodionov/redux-logger>
[redux-mock-store]: <https://github.com/arnaudbenard/redux-mock-store>
[http://localhost:3000]: <http://localhost:3000>
[npm]: <https://github.com/npm/npm>